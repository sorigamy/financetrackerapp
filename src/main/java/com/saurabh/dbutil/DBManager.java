package com.saurabh.dbutil;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.saurabh.model.Expense;

public class DBManager {
	private static BasicDataSource dataSource;
	
	static {
		ApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
		dataSource = (BasicDataSource) context.getBean("dataSource");
	}
	
	public static double getTotalExpenseSumTillToday() throws SQLException{
		Connection conn = null;
		Statement stmt = null; 
		double amount = 0;
		try {
			conn = dataSource.getConnection();
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("select sum(txn_amount) from expense_log");
			if(rs.next()){
				amount = rs.getDouble(1);
			}
			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}finally{
			if(stmt != null){
				stmt.close();
			}
			
			if(conn != null)
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
		}
		
		return amount;
	}
	
	
	public static boolean storeExpense(Expense expense) throws SQLException{
		Connection conn = null;
		CallableStatement store= null;
		boolean result = false;
		try {
			System.out.println("db storing expense " + expense);
			conn = dataSource.getConnection();
			conn.setAutoCommit(true);
			store = conn.prepareCall("{ ? = call f_insert_expense( ?, ?, ?, ? ) }");
			store.registerOutParameter(1, Types.BOOLEAN);
			store.setString(2, expense.getName());
			store.setDate(3, new java.sql.Date(expense.getDate().getTime()));
			store.setString(4, expense.getDescription());
			store.setDouble(5, expense.getAmount());
			store.execute();
			result = store.getBoolean(1);
			System.out.println("db result " + result);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}finally{
			if(store != null){
				store.close();
			}
			
			if(conn != null)
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	
		}
		
		return result;
	}
	
}
