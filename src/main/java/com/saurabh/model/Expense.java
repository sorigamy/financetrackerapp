package com.saurabh.model;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;



public class Expense {
	
	private String name;
	private String description;
	private double amount;
	
	@JsonFormat(shape=Shape.STRING, pattern="dd-MMM-yyyy")
	private Date date;
	
	public Expense() {
		// TODO Auto-generated constructor stub
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	

	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Expense [name=" + name + ", description=" + description
				+ ", amount=" + amount + ", date=" + date + "]";
	}
	
	

}
