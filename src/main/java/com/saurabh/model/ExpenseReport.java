package com.saurabh.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

public class ExpenseReport {
	private double totalAmount;
	
	@JsonFormat(shape=Shape.STRING, pattern="dd-MMM-yyyy")
	private Date todayDate;
	
	public ExpenseReport(){
		
	}
	
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public Date getTodayDate() {
		return todayDate;
	}
	public void setTodayDate(Date todayDate) {
		this.todayDate = todayDate;
	}

	@Override
	public String toString() {
		return "ExpenseReport [totalAmount=" + totalAmount + ", todayDate="
				+ todayDate + "]";
	}
	
	
}
