package com.saurabh.controller;

import java.sql.SQLException;
import java.util.Date;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.saurabh.dbutil.DBManager;
import com.saurabh.model.Expense;
import com.saurabh.model.ExpenseReport;

@Controller
public class PersonalFinanceController {
	
	@RequestMapping("/")
	public String masterHandler(){
		return "index";
	}
	
	@RequestMapping(value = "/storeExpense", method=RequestMethod.POST, consumes = "application/json")
	public @ResponseBody String storeExpense(@RequestBody Expense expense){
		System.out.println("request received with expense = " + expense);
		boolean result = false;
		try {
			result = DBManager.storeExpense(expense);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = false;
		}
		if(result) {
			System.out.println("returning success");
			return "success";
		};
		System.out.println("returning failure");
		return "failure";
	}
	
	@RequestMapping(value = "/expenseReport", produces = "application/json")
	public @ResponseBody ExpenseReport totalExpense(){
		ExpenseReport report = new ExpenseReport();
		report.setTodayDate(new Date());
		try {
			report.setTotalAmount(DBManager.getTotalExpenseSumTillToday());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			report.setTotalAmount(0);
			e.printStackTrace();
		}
		
		return report;
		
	}
	
}
