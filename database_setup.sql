DROP TABLE IF EXISTS expense_log;

CREATE TABLE expense_log (username text not null CHECK (username <> '')
, txn_date date not null CHECK (date_trunc('day',txn_date) <= current_date), txn_desc text not null CHECK (txn_desc <> ''), txn_amount double precision not null CHECK (txn_amount <> 0));


CREATE OR REPLACE FUNCTION f_insert_expense(in_username text, in_txn_date date, in_txn_desc text, in_txn_amount double precision) RETURNS boolean AS $$
DECLARE
out_result boolean;
BEGIN
	out_result := TRUE;
	BEGIN
		INSERT INTO expense_log (username, txn_date , txn_desc , txn_amount) VALUES (in_username, in_txn_date, in_txn_desc, in_txn_amount);
EXCEPTION
        WHEN OTHERS THEN 
		out_result := FALSE;
		raise notice '% %', SQLERRM, SQLSTATE; 
		return out_result;
	END;

	return out_result;
END;
$$ LANGUAGE plpgsql VOLATILE;