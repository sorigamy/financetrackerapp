# README #

This app is a personal money lending tracker. 
This app is meant to run on heroku server due to postgre configuration specific to heroku.

**This exposes 2 REST endpoints**

**1.** http://localhost:8080/storeExpense

on heroku https://financetrackerapp.herokuapp.com/storeExpense

send post request in json in below format but dont set 'Accept' in header

{
    "date": "04-May-2015",
    "name": "Someone",
    "description": "Movie ticket",
    "amount": -550.95
}

response is 'success'/'failure' depending upon success to store in db


**2.** http://localhost:8080/expenseReport

on heroku https://financetrackerapp.herokuapp.com/expenseReport

just hit the url and you will get the total money balance

{
  "totalAmount": 1100.049,
  "todayDate": "28-Jun-2015"
}



**To build this app**

1. git clone it

2. cd the folder in terminal

3. run 'mvn package'

4. run database_setup.sql in postgre

5. make changes to /src/main/resources/application-context.xml to connect to postgre

6. run 'java $JAVA_OPTS -jar target/dependency/webapp-runner.jar target/*.war'

7. for running on heroku create heroku app using 'heroku create' and run 'git push heroku master'